
var Discord = require('discord.io');
var logger = require('winston');
var request = require('request');

var auth = require('./auth.json');
var helper = require('./helper');
var helper = require('./steam');

var prefix = "&"

// Configure logger settings1
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';

// Initialize Discord Bot
var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});

bot.on('message', function (user, userID, channelID, message, evt) {

    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`
    if (helper.hasPrefix(message,prefix)) {

        var args = message.substring(prefix.length).split(' ');
        var cmd = args[0];
        args = args.splice(prefix.length);

        switch(cmd) {
            case 'ping':
                logger.info(user + " " + userID);
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!' 
                });
            break;
            
            case 'test':
                myTestFunc(args);
            break;

            case 'butter':
                bot.sendMessage({
                    to: channelID,
                    message: 'Toast.' 
                });
            break;

            default:
                bot.sendMessage({
                    to: channelID,
                    message: user + ' you dummy. \"' + cmd + '\" isn\'t a command. It doesn\'t work like that. Well.. I guess it does now.' 
                });
            break;
         }
     }
});



